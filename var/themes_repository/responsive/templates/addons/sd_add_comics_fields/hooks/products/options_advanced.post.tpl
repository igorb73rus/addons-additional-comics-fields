<div class="ty-control-group product-list-field{if !$product.art_by} hidden{/if}">
    <span class="ty-control-group__label">{__("sd_add_comics_fields.art_by")}:</span>
    <span class="ty-control-group__item" id="art_by_{$obj_id}" >{$product.art_by}</span>
</div>

<div class="ty-control-group product-list-field{if !$product.release_date} hidden{/if}">
    <span class="ty-control-group__label">{__("sd_add_comics_fields.release_date")}:</span>
    <span class="ty-control-group__item" id="release_date_{$obj_id}" >{$product.release_date|date_format:"`$settings.Appearance.date_format`"}</span>
</div>
