{** block-description:sd_add_comics_fields.written_by **}

{if $product.written_by}
    <div {live_edit name="product:written_by:{$product.product_id}"}>{$product.written_by nofilter}</div>
{/if}
