<?php

defined('BOOTSTRAP') or die('Access denied');

function fn_sd_add_comics_fields_update_product_pre(&$product_data, $product_id, $lang_code, $create)
{
    if (!empty($product_data['release_date'])) {
        $product_data['release_date'] = fn_parse_date($product_data['release_date']);
    }
}
